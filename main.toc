\beamer@sectionintoc {1}{Setting}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Information Aggregation in Spontaneous Localized Settings}{4}{0}{1}
\beamer@sectionintoc {2}{Fairness}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Selective Fairness}{6}{0}{2}
\beamer@subsubsectionintoc {2}{1}{1}{Strength of Report}{8}{0}{2}
\beamer@subsectionintoc {2}{2}{Cumulative Fairness}{9}{0}{2}
\beamer@subsubsectionintoc {2}{2}{1}{Consistency Score}{14}{0}{2}
\beamer@sectionintoc {3}{Collusion Resistivity}{16}{0}{3}
\beamer@subsectionintoc {3}{1}{Reliability Score}{19}{0}{3}
\beamer@sectionintoc {4}{Location Proof}{20}{0}{4}
\beamer@sectionintoc {5}{Final Reward}{23}{0}{5}
\beamer@sectionintoc {6}{Backup Slides}{26}{1}{6}
\beamer@subsectionintoc {6}{1}{Strength Score}{27}{1}{6}
\beamer@subsectionintoc {6}{2}{Consistency Score}{28}{1}{6}
\beamer@subsectionintoc {6}{3}{Reliability Score}{30}{1}{6}
